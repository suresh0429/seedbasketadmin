package com.suresh.firebasegrocery.Admin.Model;

public class HomeModel {

    private String id,catgoeryName,color;

    public HomeModel(String id, String catgoeryName, String color) {
        this.id = id;
        this.catgoeryName = catgoeryName;
        this.color = color;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatgoeryName() {
        return catgoeryName;
    }

    public void setCatgoeryName(String catgoeryName) {
        this.catgoeryName = catgoeryName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
