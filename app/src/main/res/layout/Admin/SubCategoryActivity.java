package com.suresh.firebasegrocery.Admin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.suresh.firebasegrocery.Admin.Adapter.SubCategoryAdapter;
import com.suresh.firebasegrocery.Admin.Model.SubCategoryModel;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubCategoryActivity extends AppCompatActivity {
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.addPost)
    FloatingActionButton addPost;
    @BindView(R.id.txtError)
    TextView txtError;


    //database reference
    private DatabaseReference mDatabase;

    private FirebaseStorage mStorage;

    //list to hold all the uploaded images
    private List<SubCategoryModel> uploads;

    String CatMKey,categoryname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        ButterKnife.bind(this);
        if (getIntent() != null){
            CatMKey = getIntent().getStringExtra("MKey");
            categoryname = getIntent().getStringExtra("CatName");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(categoryname);




        prepareCategoryData();
    }

    private void prepareCategoryData() {

        progressBar.setVisibility(View.VISIBLE);

        uploads = new ArrayList<>();

        mStorage = FirebaseStorage.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY);
        DatabaseReference subCategoriesRef = mDatabase.child(CatMKey).child(Constants.DATABASE_PATH_SUBCATEGORY);
        mDatabase.keepSynced(true);

        subCategoriesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // this dataSnapshot will have all the sermons with sermocategory as 2

                progressBar.setVisibility(View.GONE);

                Log.e("SNAPSHOT", "" + snapshot.getRef().child(""));

                if (snapshot.getValue() != null) {
                    txtError.setVisibility(View.GONE);
                    //iterating through all the values in database
                    uploads.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        SubCategoryModel upload = postSnapshot.getValue(SubCategoryModel.class);
                        upload.setmKey(postSnapshot.getKey());
                        uploads.add(upload);

                    }
                    //creating adapter
                    SubCategoryAdapter adapter = new SubCategoryAdapter(getApplicationContext(), uploads,CatMKey);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(com.suresh.firebasegrocery.Admin.SubCategoryActivity.this, RecyclerView.VERTICAL, false));
                    //adding adapter to recyclerview
                    recyclerView.setAdapter(adapter);

                    adapter.notifyDataSetChanged();


                } else {

                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Posts Added Today");
                    recyclerView.setAdapter(null);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);
            }
        });


    }


    @OnClick(R.id.addPost)
    public void onViewClicked() {

        Intent intent = new Intent(getApplicationContext(), com.suresh.firebasegrocery.Admin.SubCategoryUploadActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("MKey",CatMKey);
        intent.putExtra("CatName",categoryname);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
