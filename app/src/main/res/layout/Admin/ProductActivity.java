package com.suresh.firebasegrocery.Admin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.suresh.firebasegrocery.Admin.Adapter.ProductsAdapter;
import com.suresh.firebasegrocery.Admin.Model.ProductsModel;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductActivity extends AppCompatActivity {
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.addPost)
    FloatingActionButton addPost;
    @BindView(R.id.txtError)
    TextView txtError;


    //database reference
    private DatabaseReference mDatabase;

    private FirebaseStorage mStorage;

    //list to hold all the uploaded images
    private List<ProductsModel> uploads;

    String catMKey,subCatMKey,categoryname,subcatname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        ButterKnife.bind(this);
        if (getIntent() != null){
            catMKey = getIntent().getStringExtra("MKey");
            categoryname = getIntent().getStringExtra("CatName");
            subCatMKey = getIntent().getStringExtra("SubCatMkey");
            subcatname = getIntent().getStringExtra("SubCatName");

        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Products");




        prepareProductsData();
    }

    private void prepareProductsData() {

        progressBar.setVisibility(View.VISIBLE);

        uploads = new ArrayList<>();

        mStorage = FirebaseStorage.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY);
        DatabaseReference subCategoriesRef = mDatabase.child(catMKey).child(Constants.DATABASE_PATH_SUBCATEGORY);
        DatabaseReference productsRef = subCategoriesRef.child(subCatMKey).child(Constants.DATABASE_PATH_PRODUCTS);
        mDatabase.keepSynced(true);

        productsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // this dataSnapshot will have all the sermons with sermocategory as 2

                progressBar.setVisibility(View.GONE);

                Log.e("SNAPSHOT", "" + snapshot.getRef().child(""));

                if (snapshot.getValue() != null) {
                    txtError.setVisibility(View.GONE);
                    //iterating through all the values in database
                    uploads.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        ProductsModel upload = postSnapshot.getValue(ProductsModel.class);
                        upload.setmKey(postSnapshot.getKey());
                        uploads.add(upload);

                    }
                    //creating adapter
                    ProductsAdapter adapter = new ProductsAdapter(getApplicationContext(), uploads, catMKey,subCatMKey);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new GridLayoutManager(com.suresh.firebasegrocery.Admin.ProductActivity.this, 2));
                    //adding adapter to recyclerview
                    recyclerView.setAdapter(adapter);

                    adapter.notifyDataSetChanged();


                } else {

                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Posts Added Today");
                    recyclerView.setAdapter(null);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);
            }
        });


    }


    @OnClick(R.id.addPost)
    public void onViewClicked() {

        Intent intent = new Intent(getApplicationContext(), com.suresh.firebasegrocery.Admin.AddProductActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("MKey",catMKey);
        intent.putExtra("CatName",categoryname);
        intent.putExtra("SubCatMkey",subCatMKey);
        intent.putExtra("SubCatName",subcatname);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
