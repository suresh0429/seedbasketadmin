package com.suresh.firebasegrocery.Admin;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.suresh.firebasegrocery.Admin.Model.SubCategoryModel;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.R;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubCategoryUploadActivity extends AppCompatActivity {
    //constant to track image chooser intent
    private static final int PICK_IMAGE_REQUEST = 234;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.subcatName)
    TextInputEditText subcatName;
    @BindView(R.id.buttonUpload)
    Button buttonUpload;
    //uri to store file
    private Uri filePath;

    //firebase objects
    private StorageReference storageReference;
    private DatabaseReference mDatabase;
    private String formattedDate;
    StorageReference sRef;
    private String TAG = "SubCategoryUploadActivity";

    String MKey,categoryname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category_upload);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("SubCategory Upload");

        if (getIntent() != null){
            MKey = getIntent().getStringExtra("MKey");
            categoryname = getIntent().getStringExtra("CatName");
        }

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
/*
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
*/
        formattedDate = df.format(c.getTime());


        storageReference = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(MKey).child(Constants.DATABASE_PATH_SUBCATEGORY);


    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }


    private void uploadFile(String subcategoryName) {
        //checking if file is available

        //displaying progress dialog while image is uploading
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading");
        progressDialog.setCancelable(false);
        progressDialog.show();


        if (filePath != null) {
            //getting the storage reference
            sRef = storageReference.child(Constants.STORAGE_PATH_SUBCATEGORY + System.currentTimeMillis() + "." + getFileExtension(filePath));


        }
        else {
           /* filePath = Uri.parse("android.resource://com.suresh.rsr/drawable/" + R.drawable.ic_shopping_cart);
            Log.e("URI", "" + filePath);
            sRef = storageReference.child(Constants.STORAGE_PATH_BANNERS + "rsr_logo." + getFileExtension(filePath));
*/
            Toast.makeText(com.suresh.firebasegrocery.Admin.SubCategoryUploadActivity.this,"Select Image First",Toast.LENGTH_SHORT).show();
        }



        //adding the file to reference
        sRef.putFile(filePath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        sRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Log.d(TAG, "onSuccess: uri= " + uri.toString());

                                //dismissing the progress dialog
                                progressDialog.dismiss();

                                //adding an upload to firebase database
                                String uploadId = mDatabase.push().getKey();

                                //creating the upload object to store uploaded image details
                                SubCategoryModel upload = new SubCategoryModel(uploadId, formattedDate, "SubCategory", uri.toString(),categoryname,subcategoryName);
                                // Log.e("IMAGEURL2",""+taskSnapshot.getDownloadUrl().toString());

                                mDatabase.child(uploadId).setValue(upload);


                                Intent intent = new Intent(getApplicationContext(), com.suresh.firebasegrocery.Admin.CategoryActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();


                            }
                        });


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        //displaying the upload progress
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                    }
                });

    }


    @OnClick({R.id.imageView, R.id.buttonUpload})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageView:
                showFileChooser();
                break;
            case R.id.buttonUpload:
                String subcategoryName = subcatName.getText().toString();

                if (subcategoryName.isEmpty()){
                    subcatName.setError("Enter SubCategoryName");
                    return;

                }

                uploadFile(subcategoryName);


                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
