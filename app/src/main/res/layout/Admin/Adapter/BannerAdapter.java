package com.suresh.firebasegrocery.Admin.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.suresh.firebasegrocery.Admin.Model.BannerModel;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.Interface.OnRecyclerViewItemClickListener;
import com.suresh.firebasegrocery.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.MyViewHolder> {



    private Context mContext;
    private List<BannerModel> bannerModelList;
    private OnRecyclerViewItemClickListener listener;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.delete)
        ImageView delete;
        @BindView(R.id.cardview)
        CardView cardview;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public BannerAdapter(Context mContext, List<BannerModel> bannerModelList) {
        this.mContext = mContext;
        this.bannerModelList = bannerModelList;
    }


    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener)
    {
        this.listener = listener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.banner_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final BannerModel bannerModel = bannerModelList.get(position);

        // loading album cover using Glide library
        Glide.with(mContext).load(bannerModel.getImageUrl()).into(holder.thumbnail);
        Log.e(TAG, "onBindViewHolder: "+bannerModel.getImageUrl() );

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //creating a popup menu
                PopupMenu popup = new PopupMenu(mContext, holder.delete);
                //inflating menu from xml resource
                popup.inflate(R.menu.options_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.menu1) {//handle menu1 click
                            deleteArtist(bannerModel.getId());
                            deleteImageFromStorage(bannerModel.getmKey(), bannerModel.getImageUrl());
                            bannerModelList.remove(position);
                            notifyDataSetChanged();
                                /* case R.id.menu2:
                                //handle menu2 click
                                break;
                            case R.id.menu3:
                                //handle menu3 click
                                break;*/
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();

            }
        });




    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return bannerModelList.size();
    }


    private boolean deleteArtist(String id) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_BANNERS).child(id);

        //removing artist
        dR.removeValue();

        /*//getting the tracks reference for the specified artist
        DatabaseReference drTracks = FirebaseDatabase.getInstance().getReference("tracks").child(id);

        //removing all tracks
        drTracks.removeValue();*/

        Toast.makeText(mContext, "Post Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    private boolean deleteImageFromStorage(final String key, String imageUrl){

        FirebaseStorage mStorage = FirebaseStorage.getInstance();
        final DatabaseReference mDatabaseRef = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_BANNERS);

        StorageReference imageRef = mStorage.getReferenceFromUrl(imageUrl);
        imageRef.delete().addOnSuccessListener(aVoid -> {
            mDatabaseRef.child(key).removeValue();
            Toast.makeText(mContext, "Item deleted", Toast.LENGTH_SHORT).show();
        });
        return true;
    }
}
