package com.suresh.firebasegrocery.Admin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.suresh.firebasegrocery.Admin.Adapter.BannerAdapter;
import com.suresh.firebasegrocery.Admin.Model.BannerModel;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BannersActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.addPost)
    FloatingActionButton addPost;
    @BindView(R.id.txtError)
    TextView txtError;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    //database reference
    private DatabaseReference mDatabase;

    private FirebaseStorage mStorage;

    //list to hold all the uploaded images
    private List<BannerModel> uploads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banners);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Banners");

        prepareBannersData();
    }

    private void prepareBannersData() {

        progressBar.setVisibility(View.VISIBLE);

        uploads = new ArrayList<>();

        mStorage = FirebaseStorage.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_BANNERS);
        mDatabase.keepSynced(true);
        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                progressBar.setVisibility(View.GONE);

                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {
                    txtError.setVisibility(View.GONE);
                    //iterating through all the values in database
                    uploads.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        BannerModel upload = postSnapshot.getValue(BannerModel.class);
                        upload.setmKey(postSnapshot.getKey());
                        uploads.add(upload);

                    }
                    //creating adapter
                    BannerAdapter adapter = new BannerAdapter(getApplicationContext(), uploads);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(com.suresh.firebasegrocery.Admin.BannersActivity.this, RecyclerView.VERTICAL, false));
                    //adding adapter to recyclerview
                    recyclerView.setAdapter(adapter);

                    adapter.notifyDataSetChanged();


                } else {

                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Posts Added Today");
                    recyclerView.setAdapter(null);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    @OnClick(R.id.addPost)
    public void onViewClicked() {

        Intent intent = new Intent(getApplicationContext(), com.suresh.firebasegrocery.Admin.BannerUploadActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
