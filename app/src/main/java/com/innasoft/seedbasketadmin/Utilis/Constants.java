package com.innasoft.seedbasketadmin.Utilis;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constants {
    //Userdata
    public static final String USERDATA_PATH = "UserData";

    //Banner
    public static final String STORAGE_PATH_BANNERS = "banners/";
    public static final String DATABASE_PATH_BANNERS = "banners";


    // Category
    public static final String STORAGE_PATH_CATEGORY = "category/";
    public static final String DATABASE_PATH_CATEGORY = "category";

    // Subcategory
    public static final String STORAGE_PATH_SUBCATEGORY = "subcategory/";
    public static final String DATABASE_PATH_SUBCATEGORY = "subcategory";

    // products
    public static final String STORAGE_PATH_PRODUCT = "products/";
    public static final String DATABASE_PATH_PRODUCTS = "products";

    // payment Mode
    public static final String DATABASE_PATH_PAMENTMODE = "paymentMode";


    // cart
    public static final String DATABASE_PATH_CART = "cartList";

    // wishlist
    public static final String DATABASE_PATH_WISHLIST = "wishlist";


    public static final String CART_WISH_STATUS = "StatusCartWish";



    public static String capitalize(String capString){
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();

    }
}
