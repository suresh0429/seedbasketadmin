package com.innasoft.seedbasketadmin.Activities.Product;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.innasoft.seedbasketadmin.Activities.Category.CategoryActivity;
import com.innasoft.seedbasketadmin.Model.CategoryModel;
import com.innasoft.seedbasketadmin.Model.ProductsModel;
import com.innasoft.seedbasketadmin.R;
import com.innasoft.seedbasketadmin.Utilis.Constants;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductUpdateActivity extends AppCompatActivity {
    //constant to track image chooser intent
    private static final int PICK_IMAGE_REQUEST = 234;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.productName)
    TextInputEditText productName;
    @BindView(R.id.productWeight)
    TextInputEditText productWeight;
    @BindView(R.id.productPrice)
    TextInputEditText productPrice;
    @BindView(R.id.productAvailability)
    TextInputEditText productAvailability;
    @BindView(R.id.buttonUpload)
    Button buttonUpload;
    @BindView(R.id.productDescription)
    TextInputEditText productDescription;

    //uri to store file
    private Uri filePath;

    //firebase objects
    private StorageReference storageReference;
    private DatabaseReference mDatabase;
    private String formattedDate;
    StorageReference sRef;
    private String TAG = "ProductUpdateActivity";

    String catMKey, subCatMKey, categoryname, subcatname,imageUrl,productname,productweight,productprice,
            productavailability,description,activityStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_update);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Product");

        if (getIntent() != null) {
            catMKey = getIntent().getStringExtra("MKey");
            categoryname = getIntent().getStringExtra("CatName");
            subCatMKey = getIntent().getStringExtra("SubCatMkey");
            subcatname = getIntent().getStringExtra("SubCatName");

            imageUrl = getIntent().getStringExtra("imageUrl");
            productname = getIntent().getStringExtra("productName");
            productweight = getIntent().getStringExtra("productWeight");
            productprice = getIntent().getStringExtra("productPrice");
            productavailability = getIntent().getStringExtra("productAvailability");
            description = getIntent().getStringExtra("description");
            activityStatus = getIntent().getStringExtra("ActivityStatus");

            Glide.with(this).load(imageUrl).into(imageView);
            productName.setText(productname);
            productWeight.setText(productweight);
            productPrice.setText(productprice);
            productAvailability.setText(productavailability);
            productDescription.setText(description);



        }

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());


        storageReference = FirebaseStorage.getInstance().getReference();
      //  mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(catMKey).child(Constants.DATABASE_PATH_SUBCATEGORY).child(subCatMKey).child(Constants.DATABASE_PATH_PRODUCTS);
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_PRODUCTS).child(catMKey).child(subCatMKey);


        productAvailability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dilogue();
            }
        });

    }

    private void dilogue(){
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(ProductUpdateActivity.this).inflate(R.layout.my_location_lis_dialog, viewGroup, false);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(ProductUpdateActivity.this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();

        ListView recyclerView = (ListView) dialogView.findViewById(R.id.locationRecycler);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("In Stock");
        arrayList.add("Out Of Stock");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProductUpdateActivity.this,android.R.layout.simple_list_item_1,arrayList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        recyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String listItem = recyclerView.getItemAtPosition(i).toString();
                productAvailability.setText(listItem);
                alertDialog.dismiss();
            }
        });
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }


    private void uploadFile(String productname, String productweight, String productprice, String productAvalibulity, String productdescription) {
        //checking if file is available

        //displaying progress dialog while image is uploading
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading");
        progressDialog.setCancelable(false);
        progressDialog.show();


        if (filePath != null) {
            //getting the storage reference
            sRef = storageReference.child(Constants.STORAGE_PATH_PRODUCT + System.currentTimeMillis() + "." + getFileExtension(filePath));


        } else {
           /* filePath = Uri.parse("android.resource://com.suresh.rsr/drawable/" + R.drawable.ic_shopping_cart);
            Log.e("URI", "" + filePath);
            sRef = storageReference.child(Constants.STORAGE_PATH_BANNERS + "rsr_logo." + getFileExtension(filePath));
*/
            Toast.makeText(ProductUpdateActivity.this, "Select Image First", Toast.LENGTH_SHORT).show();
        }


        //adding the file to reference
        sRef.putFile(filePath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        sRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Log.d(TAG, "onSuccess: uri= " + uri.toString());

                                //dismissing the progress dialog
                                progressDialog.dismiss();

                                //adding an upload to firebase database
                                String uploadId = catMKey;

                                //creating the upload object to store uploaded image details
                                ProductsModel upload = new ProductsModel("",uploadId, formattedDate, "Products", uri.toString(), categoryname, subcatname, productname, productprice,productweight+" Seeds", productAvalibulity,productdescription);
                                // Log.e("IMAGEURL2",""+taskSnapshot.getDownloadUrl().toString());

                                mDatabase.child(uploadId).setValue(upload);


                                Intent intent = new Intent(getApplicationContext(), ProductActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("MKey",catMKey);
                                intent.putExtra("CatName",categoryname);
                                intent.putExtra("SubCatMkey",subCatMKey);
                                intent.putExtra("SubCatName",subcatname);
                                startActivity(intent);
                                Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();


                            }
                        });


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        //displaying the upload progress
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                    }
                });

    }


    private void updateFile(String productname, String productweight, String productprice, String productAvalibulity, String productdescription){
        //creating the upload object to store uploaded image details
        ProductsModel upload = new ProductsModel("",catMKey, formattedDate, "Products",imageUrl, categoryname, subcatname, productname, productprice,productweight+" Seeds", productAvalibulity,productdescription);
        // Log.e("IMAGEURL2",""+taskSnapshot.getDownloadUrl().toString());

        mDatabase.child(catMKey).setValue(upload);


        Intent intent = new Intent(getApplicationContext(), ProductActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("MKey",catMKey);
        intent.putExtra("CatName",categoryname);
        intent.putExtra("SubCatMkey",subCatMKey);
        intent.putExtra("SubCatName",subcatname);
        startActivity(intent);
        Toast.makeText(getApplicationContext(), "Product Updated ", Toast.LENGTH_LONG).show();

    }
    @OnClick({R.id.imageView, R.id.buttonUpload})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageView:
                showFileChooser();
                break;
            case R.id.buttonUpload:
                String productname = productName.getText().toString();
                String productweight = productWeight.getText().toString();
                String productprice = productPrice.getText().toString();
                String productavalibulity = productAvailability.getText().toString();
                String productdescription = productDescription.getText().toString();

                if (productname.isEmpty()) {
                    productName.setError("Enter ProductName");
                    return;

                }
                if (productweight.isEmpty()) {
                    productWeight.setError("Enter Product Weight");
                    return;

                }
                if (productprice.isEmpty()) {
                    productPrice.setError("Enter Product Price");
                    return;

                }
                if (productavalibulity.isEmpty()) {
                    productAvailability.setError("Enter Product Availability");
                    return;

                }
                if (productdescription.isEmpty()) {
                    productAvailability.setError("Enter Product Description");
                    return;

                }

                if (filePath != null) {
                    uploadFile(productname, productweight, productprice, productavalibulity,productdescription);

                } else {
                    updateFile(productname, productweight, productprice, productavalibulity,productdescription);
                }



                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
