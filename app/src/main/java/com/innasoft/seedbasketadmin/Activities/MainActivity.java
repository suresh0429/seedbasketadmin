package com.innasoft.seedbasketadmin.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.innasoft.seedbasketadmin.Adapter.AdminHomeAdapter;
import com.innasoft.seedbasketadmin.Interface.PaymentTypeInterface;
import com.innasoft.seedbasketadmin.Model.HomeModel;
import com.innasoft.seedbasketadmin.Model.PaymentModeModel;
import com.innasoft.seedbasketadmin.R;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.innasoft.seedbasketadmin.Utilis.Constants.DATABASE_PATH_PAMENTMODE;


public class MainActivity extends Activity implements PaymentTypeInterface {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private List<HomeModel> homeModelList = new ArrayList<>();
    PaymentTypeInterface paymentTypeInterface;
    String cod,online;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        paymentTypeInterface = (PaymentTypeInterface)this;
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        AdminHomeAdapter adapter = new AdminHomeAdapter(this, homeModelList, paymentTypeInterface);
        recyclerView.setAdapter(adapter);



        adminModuleData();

    }

    private void adminModuleData() {
        HomeModel homeModel = new HomeModel("1", "Add Home Banners", "#FFC2185B");
        homeModelList.add(homeModel);

        homeModel = new HomeModel("2", "Add category", "#FF7B1FA2");
        homeModelList.add(homeModel);

        homeModel = new HomeModel("3", "Add Subcategory", "#D84315");
        homeModelList.add(homeModel);

        homeModel = new HomeModel("4", "Add Product", "#FF303F9F");
        homeModelList.add(homeModel);

        homeModel = new HomeModel("5", "Add Delivery Type", "#FF7B1FA2");
        homeModelList.add(homeModel);

    }

    @Override
    public void onpaymentType(Context mContext) {

        SharedPreferences preferences = getSharedPreferences("PAYMENTMODE",0);
        SharedPreferences.Editor editor = preferences.edit();


        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomAlertDialog);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();


        Switch tbCod = (Switch) dialogView.findViewById(R.id.tbCod);
        Switch tbOnline = (Switch) dialogView.findViewById(R.id.tbOnline);

        SharedPreferences preferences1 = getSharedPreferences("PAYMENTMODE",0);
        String one = preferences1.getString("COD","");
        String two = preferences1.getString("ONLINE","");

        if (one.equalsIgnoreCase("Pay on Delivery")){
            tbCod.setChecked(true);
        }

        if (two.equalsIgnoreCase("Pay at Online")){
            tbOnline.setChecked(true);
        }

        tbCod.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b){
                    cod = "Pay on Delivery";
                    updatePaymentMode(cod,b,"1","PAYMENT0001","https://lh3.googleusercontent.com/-GAbv3fNjJXE/XaAbh7bFeNI/AAAAAAAABDk/iE8sn-M-vXIWnkBIAiwoWnMQW7_bZAE5QCK8BGAsYHg/s0/2019-10-10.png");
                }
                else {
                    cod ="";
                    updatePaymentMode(cod,b,"1","PAYMENT0001","https://lh3.googleusercontent.com/-GAbv3fNjJXE/XaAbh7bFeNI/AAAAAAAABDk/iE8sn-M-vXIWnkBIAiwoWnMQW7_bZAE5QCK8BGAsYHg/s0/2019-10-10.png");
                }

                editor.putString("COD",cod);
                editor.apply();
            }
        });

        tbOnline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    online = "Pay at Online";
                    updatePaymentMode(online,b,"2","PAYMENT0002","https://lh3.googleusercontent.com/-kw4gOv2svKA/XaAbjaqeLxI/AAAAAAAABDo/FOQFx-myXKIyorkLgljfZJQmbJQexpTAACK8BGAsYHg/s0/2019-10-10.png");
                }else {
                    online = "";
                    updatePaymentMode(online,b,"2","PAYMENT0002","https://lh3.googleusercontent.com/-kw4gOv2svKA/XaAbjaqeLxI/AAAAAAAABDo/FOQFx-myXKIyorkLgljfZJQmbJQexpTAACK8BGAsYHg/s0/2019-10-10.png");
                }

                editor.putString("ONLINE",online);
                editor.apply();
            }
        });


        Button buttonOk = (Button) dialogView.findViewById(R.id.btnSubmit);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // updatePaymentMode();
                alertDialog.dismiss();
            }
        });

        //finally creating the alert dialog and displaying it





    }



    private void updatePaymentMode(String paymentType, boolean b, String paymentID, String payment,String image) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_PAMENTMODE).child(payment);
        PaymentModeModel product = new PaymentModeModel();
        //Adding values
        product.setPaymentId(paymentID);
        product.setChecked(b);
        product.setPaymentType(paymentType);
        product.setImage(image);
        mDatabase.setValue(product);

    }

}
