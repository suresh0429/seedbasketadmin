package com.innasoft.seedbasketadmin.Activities.Category;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.innasoft.seedbasketadmin.Adapter.CategoryAdapter;
import com.innasoft.seedbasketadmin.Utilis.Constants;
import com.innasoft.seedbasketadmin.Model.CategoryModel;
import com.innasoft.seedbasketadmin.R;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryActivity extends AppCompatActivity {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.addPost)
    FloatingActionButton addPost;
    @BindView(R.id.txtError)
    TextView txtError;


    //database reference
    private DatabaseReference mDatabase;

    private FirebaseStorage mStorage;

    //list to hold all the uploaded images
    private List<CategoryModel> uploads;

    String activityStatus ;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Category");

        if (getIntent() != null){
           activityStatus = getIntent().getStringExtra("ActivityStatus");
        }

        prepareCategoryData();

        if (activityStatus.equalsIgnoreCase("SubCategoryActivity") || activityStatus.equalsIgnoreCase("ProductActivity")){

            addPost.setVisibility(View.GONE);

        }
    }

    private void prepareCategoryData() {

        progressBar.setVisibility(View.VISIBLE);

        uploads = new ArrayList<>();

        mStorage = FirebaseStorage.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY);
        mDatabase.keepSynced(true);
        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                progressBar.setVisibility(View.GONE);

                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {
                    txtError.setVisibility(View.GONE);
                    //iterating through all the values in database
                    uploads.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        CategoryModel upload = postSnapshot.getValue(CategoryModel.class);
                        upload.setmKey(postSnapshot.getKey());
                        uploads.add(upload);

                    }
                    //creating adapter
                    CategoryAdapter adapter = new CategoryAdapter(getApplicationContext(), uploads,activityStatus);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(CategoryActivity.this, RecyclerView.VERTICAL, false));
                    //adding adapter to recyclerview
                    recyclerView.setAdapter(adapter);

                    adapter.notifyDataSetChanged();


                } else {

                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Posts Added Today. Please Add Categories.");
                    recyclerView.setAdapter(null);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    @OnClick(R.id.addPost)
    public void onViewClicked() {

        Intent intent = new Intent(getApplicationContext(), CategoryUploadActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("ActivityStatus",activityStatus);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
