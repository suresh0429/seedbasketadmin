package com.innasoft.seedbasketadmin.Interface;

public interface OnRecyclerViewItemClickListener {

    public void onRecyclerViewItemClicked(int position, int id);

}
