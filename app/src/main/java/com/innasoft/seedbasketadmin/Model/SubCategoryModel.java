package com.innasoft.seedbasketadmin.Model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class SubCategoryModel {
    public String id;
    public String currentDate;
    public String imageType;
    public String imageUrl;
    public String category;
    public String subcategory;
    private String mKey;

    public SubCategoryModel() {
    }

    public SubCategoryModel(String id, String currentDate, String imageType, String imageUrl, String category, String subcategory) {
        this.id = id;
        this.currentDate = currentDate;
        this.imageType = imageType;
        this.imageUrl = imageUrl;
        this.category = category;
        this.subcategory = subcategory;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    @Exclude
    public String getmKey() {
        return mKey;
    }

    @Exclude
    public void setmKey(String mKey) {
        this.mKey = mKey;
    }
}
