package com.innasoft.seedbasketadmin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.innasoft.seedbasketadmin.Activities.Product.ProductUpdateActivity;
import com.innasoft.seedbasketadmin.Activities.SubCategory.SubCategoryUpdateActivity;
import com.innasoft.seedbasketadmin.Utilis.Constants;
import com.innasoft.seedbasketadmin.Model.ProductsModel;
import com.innasoft.seedbasketadmin.R;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.innasoft.seedbasketadmin.Utilis.Constants.capitalize;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {


    private Context mContext;
    private List<ProductsModel> productsModelList;
    private String catMKey,subCatMKey,activityStatus;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.linear)
        LinearLayout linear;
        @BindView(R.id.delete)
        ImageView delete;
        @BindView(R.id.cardview)
        CardView cardview;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public ProductsAdapter(Context mContext, List<ProductsModel> productsModelList, String catMKey, String subCatMKey, String activityStatus) {
        this.mContext = mContext;
        this.productsModelList = productsModelList;
        this.catMKey = catMKey;
        this.subCatMKey = subCatMKey;
        this.activityStatus = activityStatus;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductsModel productsModel = productsModelList.get(position);

        // loading album cover using Glide library
        Glide.with(mContext).load(productsModel.getImageUrl()).into(holder.thumbnail);
        Log.e(TAG, "onBindViewHolder: " + productsModel.getImageUrl());

        holder.txtTitle.setText(capitalize(productsModel.getProductName()));
        holder.txtPrice.setText(mContext.getResources().getString(R.string.Rs)+productsModel.getProductPrice()+" ("+productsModel.getCapacityWeight()+")");

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(mContext, holder.delete);
                //inflating menu from xml resource
                popup.inflate(R.menu.options_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.menu1) {//handle menu1 click
                            deleteArtist(productsModel.getmKey());
                            deleteImageFromStorage(productsModel.getmKey(), productsModel.getImageUrl());
                            productsModelList.remove(position);
                            notifyDataSetChanged();

                        }else if (item.getItemId() == R.id.menu2){
                            Intent intent = new Intent(mContext, ProductUpdateActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("MKey",productsModel.getmKey());
                            intent.putExtra("CatName",productsModel.getCategory());
                            intent.putExtra("SubCatName",productsModel.getSubcategory());
                            intent.putExtra("SubCatMkey",subCatMKey);
                            intent.putExtra("imageUrl",productsModel.getImageUrl());
                            intent.putExtra("productName",productsModel.getProductName());
                            intent.putExtra("productWeight",productsModel.getCapacityWeight());
                            intent.putExtra("productPrice",productsModel.getProductPrice());
                            intent.putExtra("productAvailability",productsModel.getAvailability());
                            intent.putExtra("description",productsModel.getDescription());
                            intent.putExtra("ActivityStatus",activityStatus);
                            mContext.startActivity(intent);


                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();

            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return productsModelList.size();
    }


    private boolean deleteArtist(String id) {
        //getting the specified artist reference
       // DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_PRODUCTS).child(id);

        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(catMKey).child(Constants.DATABASE_PATH_SUBCATEGORY).child(subCatMKey).child(Constants.STORAGE_PATH_PRODUCT).child(id);

        //removing artist
        dR.removeValue();



        Toast.makeText(mContext, "Post Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    private boolean deleteImageFromStorage(final String key, String imageUrl) {

        FirebaseStorage mStorage = FirebaseStorage.getInstance();
        final DatabaseReference mDatabaseRef = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_PRODUCTS);

        StorageReference imageRef = mStorage.getReferenceFromUrl(imageUrl);
        imageRef.delete().addOnSuccessListener(aVoid -> {
            mDatabaseRef.child(key).removeValue();
            Toast.makeText(mContext, "Item deleted", Toast.LENGTH_SHORT).show();
        });
        return true;
    }
}
