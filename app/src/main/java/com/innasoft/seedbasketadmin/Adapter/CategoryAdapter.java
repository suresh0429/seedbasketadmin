package com.innasoft.seedbasketadmin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.innasoft.seedbasketadmin.Activities.Banner.BannerUpdateActivity;
import com.innasoft.seedbasketadmin.Activities.Category.CategoryUpdateActivity;
import com.innasoft.seedbasketadmin.Utilis.Constants;
import com.innasoft.seedbasketadmin.Model.CategoryModel;
import com.innasoft.seedbasketadmin.Interface.OnRecyclerViewItemClickListener;
import com.innasoft.seedbasketadmin.R;
import com.innasoft.seedbasketadmin.Activities.SubCategory.SubCategoryActivity;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<CategoryModel> bannerModelList;
    private OnRecyclerViewItemClickListener listener;
    private String activityStatus;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.cardview)
        CardView cardview;
        @BindView(R.id.delete)
        ImageView delete;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public CategoryAdapter(Context mContext, List<CategoryModel> bannerModelList, String activityStatus) {
        this.mContext = mContext;
        this.bannerModelList = bannerModelList;
        this.activityStatus = activityStatus;
    }


    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.listener = listener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cat_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CategoryModel bannerModel = bannerModelList.get(position);


        // loading album cover using Glide library
        Glide.with(mContext).load(bannerModel.getImageUrl()).into(holder.thumbnail);
        Log.e(TAG, "onBindViewHolder: " + bannerModel.getImageUrl());

        holder.txtTitle.setText(bannerModel.getCategory());
        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (activityStatus.equalsIgnoreCase("SubCategoryActivity") || activityStatus.equalsIgnoreCase("ProductActivity")){
                    Intent intent = new Intent(mContext, SubCategoryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("MKey",bannerModel.getmKey());
                    intent.putExtra("CatName",bannerModel.getCategory());
                    intent.putExtra("ActivityStatus",activityStatus);
                    mContext.startActivity(intent);
                }

            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(mContext, holder.delete);
                //inflating menu from xml resource
                popup.inflate(R.menu.options_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.menu1) {//handle menu1 click
                            deleteArtist(bannerModel.getId());
                            deleteImageFromStorage(bannerModel.getmKey(), bannerModel.getImageUrl());
                            bannerModelList.remove(position);
                            notifyDataSetChanged();
                                /* case R.id.menu2:
                                //handle menu2 click
                                break;
                            case R.id.menu3:
                                //handle menu3 click
                                break;*/
                        }else if (item.getItemId() == R.id.menu2){
                            Intent intent = new Intent(mContext, CategoryUpdateActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("MKey",bannerModel.getmKey());
                            intent.putExtra("imageUrl",bannerModel.getImageUrl());
                            intent.putExtra("category",bannerModel.getCategory());
                            intent.putExtra("ActivityStatus",activityStatus);
                            mContext.startActivity(intent);
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();

            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return bannerModelList.size();
    }


    private boolean deleteArtist(String id) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(id);


        //removing artist
        dR.removeValue();

        /*//getting the tracks reference for the specified artist
        DatabaseReference drTracks = FirebaseDatabase.getInstance().getReference("tracks").child(id);

        //removing all tracks
        drTracks.removeValue();*/

        Toast.makeText(mContext, "Post Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    private boolean deleteImageFromStorage(final String key, String imageUrl) {

        FirebaseStorage mStorage = FirebaseStorage.getInstance();
        final DatabaseReference mDatabaseRef = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY);

        StorageReference imageRef = mStorage.getReferenceFromUrl(imageUrl);
        imageRef.delete().addOnSuccessListener(aVoid -> {
            mDatabaseRef.child(key).removeValue();
            Toast.makeText(mContext, "Item deleted", Toast.LENGTH_SHORT).show();
        });
        return true;
    }
}
