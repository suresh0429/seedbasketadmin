package com.innasoft.seedbasketadmin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.innasoft.seedbasketadmin.Activities.Category.CategoryUpdateActivity;
import com.innasoft.seedbasketadmin.Activities.SubCategory.SubCategoryUpdateActivity;
import com.innasoft.seedbasketadmin.Utilis.Constants;
import com.innasoft.seedbasketadmin.Model.SubCategoryModel;
import com.innasoft.seedbasketadmin.Interface.OnRecyclerViewItemClickListener;
import com.innasoft.seedbasketadmin.Activities.Product.ProductActivity;
import com.innasoft.seedbasketadmin.R;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<SubCategoryModel> bannerModelList;
    private OnRecyclerViewItemClickListener listener;
    private String catMKey;
    private String activityStatus;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.cardview)
        CardView cardview;
        @BindView(R.id.delete)
        ImageView delete;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public SubCategoryAdapter(Context mContext, List<SubCategoryModel> bannerModelList, String catMKey, String activityStatus) {
        this.mContext = mContext;
        this.bannerModelList = bannerModelList;
        this.catMKey = catMKey;
        this.activityStatus = activityStatus;
    }


    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.listener = listener;
    }


    @Override
    public SubCategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cat_card, parent, false);

        return new SubCategoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SubCategoryAdapter.MyViewHolder holder, final int position) {
        final SubCategoryModel bannerModel = bannerModelList.get(position);

        // loading album cover using Glide library
        Glide.with(mContext).load(bannerModel.getImageUrl()).into(holder.thumbnail);
        Log.e(TAG, "onBindViewHolder: " + bannerModel.getImageUrl());

        holder.txtTitle.setText(bannerModel.getSubcategory());
        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (activityStatus.equalsIgnoreCase("ProductActivity")) {
                    Intent intent = new Intent(mContext, ProductActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("MKey", catMKey);
                    intent.putExtra("CatName", bannerModel.getCategory());
                    intent.putExtra("SubCatMkey", bannerModel.getmKey());
                    intent.putExtra("SubCatName", bannerModel.getSubcategory());
                    intent.putExtra("ActivityStatus",activityStatus);
                    mContext.startActivity(intent);
                }

            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(mContext, holder.delete);
                //inflating menu from xml resource
                popup.inflate(R.menu.options_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.menu1) {//handle menu1 click
                            deleteArtist(bannerModel.getmKey());
                            deleteImageFromStorage(bannerModel.getmKey(), bannerModel.getImageUrl());
                            bannerModelList.remove(position);
                            notifyDataSetChanged();

                        }else if (item.getItemId() == R.id.menu2){
                            Intent intent = new Intent(mContext, SubCategoryUpdateActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("MKey",bannerModel.getmKey());
                            intent.putExtra("imageUrl",bannerModel.getImageUrl());
                            intent.putExtra("CatName",bannerModel.getCategory());
                            intent.putExtra("subcategory",bannerModel.getSubcategory());
                            intent.putExtra("ActivityStatus",activityStatus);
                            mContext.startActivity(intent);
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();

            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return bannerModelList.size();
    }


    private boolean deleteArtist(String id) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(catMKey).child(Constants.DATABASE_PATH_SUBCATEGORY).child(id);


        //removing artist
        dR.removeValue();

        /*//getting the tracks reference for the specified artist
        DatabaseReference drTracks = FirebaseDatabase.getInstance().getReference("tracks").child(id);

        //removing all tracks
        drTracks.removeValue();*/

        Toast.makeText(mContext, "Post Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    private boolean deleteImageFromStorage(final String key, String imageUrl) {

        FirebaseStorage mStorage = FirebaseStorage.getInstance();
        final DatabaseReference mDatabaseRef = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_SUBCATEGORY);

        StorageReference imageRef = mStorage.getReferenceFromUrl(imageUrl);
        imageRef.delete().addOnSuccessListener(aVoid -> {
            mDatabaseRef.child(key).removeValue();
            Toast.makeText(mContext, "Item deleted", Toast.LENGTH_SHORT).show();
        });
        return true;
    }
}
