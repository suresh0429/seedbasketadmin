package com.innasoft.seedbasketadmin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.seedbasketadmin.Activities.Banner.BannersActivity;
import com.innasoft.seedbasketadmin.Activities.Category.CategoryActivity;
import com.innasoft.seedbasketadmin.Interface.PaymentTypeInterface;
import com.innasoft.seedbasketadmin.Model.HomeModel;
import com.innasoft.seedbasketadmin.R;




import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdminHomeAdapter extends RecyclerView.Adapter<AdminHomeAdapter.MyViewHolder> {


    private Context mContext;
    private List<HomeModel> homeList;
    private PaymentTypeInterface paymentType;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.cardview)
        CardView cardview;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);

        }
    }

    public AdminHomeAdapter(Context mContext, List<HomeModel> homekitchenList, PaymentTypeInterface paymentType) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
        this.paymentType = paymentType;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final HomeModel home = homeList.get(position);

        holder.txtTitle.setText(home.getCatgoeryName());
        holder.cardview.setCardBackgroundColor(Color.parseColor(home.getColor()));

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position==0){
                    Intent intent = new Intent(mContext, BannersActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);

                }else if (position==1){
                    Intent intent = new Intent(mContext, CategoryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("ActivityStatus","CategoryActivity");
                    mContext.startActivity(intent);

                }else if (position==2){
                    Intent intent = new Intent(mContext, CategoryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("ActivityStatus","SubCategoryActivity");
                    mContext.startActivity(intent);

                }else if (position==3){
                    Intent intent = new Intent(mContext, CategoryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("ActivityStatus","ProductActivity");
                    mContext.startActivity(intent);
                }
                else if (position==4){
                    paymentType.onpaymentType(mContext);
                }
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


}
